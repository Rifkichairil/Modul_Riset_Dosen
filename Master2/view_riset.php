<?php
    include_once 'top.php';
        //panggil file untuk operasi db

    require_once 'db/class_riset_dosen.php';
    $obj_riset = new Riset_dosen();
        //buat variabel utk menyimpan id

    $_id = $_GET['id'];
        //buat variabel untuk mengambil id

    $data = $obj_riset->findByID($_id);
?>

<div class="row">
	<div class="col-md-12">
 		<div class="panel panel-default">
 			<div class="panel-heading">
 				<h3 class="panel-title">View Riset Dosen</h3>
 			</div>
 		
 		<div class="panel-body">
 			<table class="table">

 			  <tr>
 				<td class="active">ID Dosen</td>
 				<td>:</td>
 				<td><?php echo $data['dosen_id']?></td>
			 </tr>
		  
             <tr>
                <td class="active">Judu;</td>
                <td>:</td>
                <td><?php echo $data['judul']?></td>
             </tr>
                          <tr>
                <td class="active">Sumber Dana</td>
                <td>:</td>
                <td><?php echo $data['sumber_pendanaan']?></td>
             </tr>
                          <tr>
                <td class="active">Biaya</td>
                <td>:</td>
                <td><?php echo $data['biaya']?></td>
             </tr>
                          <tr>
                <td class="active">Mulai Semester</td>
                <td>:</td>
                <td><?php echo $data['mulai_semester']?></td>
             </tr>
                          <tr>
                <td class="active">Akhir Semester</td>
                <td>:</td>
                <td><?php echo $data['akhir_semester']?></td>
             </tr>
                          <tr>
                <td class="active">Deskripsi</td>
                <td>:</td>
                <td><?php echo $data['deskripsi']?></td>
             </tr>
                          <tr>
                <td class="active">ID Kategori</td>
                <td>:</td>
                <td><?php echo $data['kategori_id']?></td>
             </tr>
		 	</table>
 		</div>

 
 	<div class="panel-footer">
 		<a class="btn icon-btn btn-success" href="form_riset.php">
 			<span class="glyphicon btn-glyphicon glyphicon-plus imgcircle text-success"></span>
		 Tambah Riset
		</a>
 
             	  </div>
 			</div>
	  </div>
</div>


<?php
include_once 'bottom.php';
?>
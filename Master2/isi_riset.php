
<?php
    include_once 'top.php';

    
    require_once 'db/class_riset_dosen.php';
    
?>

<h2>Daftar Riset Dosen </h2>
    <div class="panel-header">
        <a class="btn icon-btn btn-success" href="form_riset.php">
        <span class="glyphicon btn-glyphicon glyphicon-plus img-
        circle text-success"></span>
        Tambah Riset Dosen
        </a>
    </div>


<?php
    $obj_riset = new Riset_dosen();
    $rows = $obj_riset->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable -->


<script language="JavaScript">
 $(document).ready(function() {
 $('#example').DataTable();
 } );
</script>

<table id="example" class="table table-striped table-bordered">

        <thead>
            <tr class="active">
             <th>Id</th>
             <th>ID Dosen</th>
             <th>Judul</th>
             <th>Sumber Dana</th>
             <th>Biaya</th>
             <th>Mulai Semester</th>
             <th>Akhir Semester</th>
             <th>Deskripsi</th>
             <th>ID Kategori</th>
             <th>Action</th>
            </tr>
    
        </thead>
    <tbody>

    <?php

    $nomor = 1;

    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['dosen_id'].'</td>';
        echo '<td>'.$row['judul'].'</td>';
        echo '<td>'.$row['sumber_pendanaan'].'</td>';
        echo '<td>'.$row['biaya'].'</td>';
        echo '<td>'.$row['mulai_semester'].'</td>';
        echo '<td>'.$row['akhir_semester'].'</td>';
        echo '<td>'.$row['deskripsi'].'</td>';
        echo '<td>'.$row['kategori_id'].'</td>';

        
        echo '<td><a href="view_riset.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_riset.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
    include_once 'bottom.php';
?>
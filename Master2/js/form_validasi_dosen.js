$(function(){

	$("form[name='form_dosen']").validate({
		rules: {
			nidn:{
				required:true,
				maxlenght:10,
			},
			nama:"required",
			gelar_belakang:"required",
			jk:"required",
			prodi_id:"required",
			jabatan_id:"required",
			pend_akhir:"required",
		},
		messages:{
			nidn:{
				required:"Tolong diisi",
				maxlenght:"Maximum 10 angka",
			},
			nama:"Tolong diisi jangan sampai kosong",
			gelar_belakang:"Tolong diisi jangan sampai kosong",
			jk:"Tolong diisi jangan sampai kosong",
			prodi_id:"Tolong diisi jangan sampai kosong",
			jabatan_id:"Tolong diisi jangan sampai kosong",
			pend_akhir:"Tolong diisi jangan sampai kosong",
		},
		submitHandler : function(form){
			form.submit();
		}
	});
});
<?php
    include_once 'top.php';

    
    require_once 'db/class_kategori_riset.php';
    
?>

<h2>Daftar Kategori Riset </h2>
    <div class="panel-header">
        <a class="btn icon-btn btn-success" href="form_kategori.php">
        <span class="glyphicon btn-glyphicon glyphicon-plus img-
        circle text-success"></span>
        Tambah Kategori Riset
        </a>
    </div>


<?php
    $obj_kategori = new Kategori_riset();
    $rows = $obj_kategori->getAll();
?>
<!-- Buat code javascript untuk memanggil table dan menggunakan fungsi datatable -->

<script language="JavaScript">
 $(document).ready(function() {
 $('#example').DataTable();
 } );
</script>

<table id="example" class="table table-striped table-bordered"><!-- Beri id pada tag table untuk dideteksi javascript-->
    
        <thead>
            <tr class="active">
             <th>Id</th>
             <th>Nama</th>
             
             <th>Action</th>
            </tr>
    
        </thead>
    <tbody>

    <?php

    $nomor = 1;

    foreach($rows as $row){
        echo '<tr><td>'.$nomor.'</td>';
        echo '<td>'.$row['nama'].'</td>';
       
        
        echo '<td><a href="view_kategori.php?id='.$row['id']. '">View</a> |';
        echo '<a href="form_kategori.php?id='.$row['id']. '">Update</a></td>';
        echo '</tr>';
        $nomor++;
    }
    ?>
    </tbody>
</table>
<?php
    include_once 'bottom.php';
?>
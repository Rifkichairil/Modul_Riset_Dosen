<?php
	include_once 'top.php';
      //panggil file yang melakukan operasi db

	require_once 'db/class_riset_dosen.php';

	$obj_riset = new Riset_dosen();
    //buat variabel untuk memanggil class

	$_idedit = $_GET['id'];
    //buat variabel utk menyimpan id

	if(!empty($_idedit)){
		$data = $obj_riset->findByID($_idedit);
    //buat pengecekan apakah datanya ada atau tidak

	}else{
		$data = [];
	}
?>

<script src="js/form_validasi_riset.js"></script>


<form class="form-horizontal" method="POST" name="form_riset" action="proses_riset.php">


<!-- Form Name -->
<legend>Form Riset Dosen</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="dosen_id">ID Dosen</label>  
  <div class="col-md-4">
  <input id="dosen_id" name="dosen_id" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['dosen_id']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="judul">Judul</label>  
  <div class="col-md-4">
  <input id="judul" name="judul" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['judul']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="sumber_pendanaan">Sumber Dana</label>  
  <div class="col-md-4">
  <input id="sumber_pendanaan" name="sumber_pendanaan" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['sumber_pendanaan']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="biaya">Biaya</label>  
  <div class="col-md-4">
  <input id="biaya" name="biaya" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['biaya']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="mulai_semester">Mulai Semester</label>  
  <div class="col-md-4">
  <input id="mulai_semester" name="mulai_semester" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['mulai_semester']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="akhir_semester">Akhir Semester</label>  
  <div class="col-md-4">
  <input id="akhir_semester" name="akhir_semester" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['akhir_semester']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="deskripsi">Deskripsi</label>  
  <div class="col-md-4">
  <input id="deskripsi" name="deskripsi" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['deskripsi']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="kategori_id">ID Kategori</label>  
  <div class="col-md-4">
  <input id="kategori_id" name="kategori_id" type="text" placeholder="" class="form-control input-md">
    
  </div>
</div>


<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">


  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>

    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>

  </div>
</div>

</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>


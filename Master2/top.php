<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Modul Riset</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

	<link href="css/bootstrap_united.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="css/style.css" rel="stylesheet">
   
    <script src="js/jquery.min.js"></script>
 	<script src="js/bootstrap.min.js"></script>
 	<script src="js/scripts.js"></script>

    <script src="js/jquery.validate.js"></script>	
    
    <link rel="stylesheet" type="text/css"
    href="css/dataTables.bootstrap.min.css"/>

    <script type="text/javascript" charset="utf8"
	src="js/jquery.dataTables.min.js"></script>

	
  </head>
    <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					 
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button> 
					<a class="navbar-brand" href="index.php">Modul Riset Dosen</a>
					<a class="navbar-brand" href="../Master/b.php">Modul Dosen</a>
				</div>
				
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						
						
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Grafik<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="../Master/grafik_master.php">Modul Dosen</a>
								</li>
								<li>
									<a href="../Master/isi_prodi.php">Tabel Prodi</a>
								</li>
								<li>
									<a href="../Master/isi_rombel.php">Tabel Rombel</a>
								</li>
							</ul>
						</li>
						<li class="dropdown">
							 <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tabel Riset Dosen<strong class="caret"></strong></a>
							<ul class="dropdown-menu">
								<li>
									<a href="isi_kategori.php">Tabel Kategori Riset</a>
								</li>
								<li>
									<a href="isi_riset.php">Tabel Riset Dosen</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				
			</nav>
		</div>
	</div>

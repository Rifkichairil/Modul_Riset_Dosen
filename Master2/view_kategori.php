<?php
    include_once 'top.php';
        //panggil file untuk operasi db

    require_once 'db/class_kategori_riset.php';
    $obj_kategori = new Kategori_riset();
        //buat variabel utk menyimpan id

    $_id = $_GET['id'];
        //buat variabel untuk mengambil id

    $data = $obj_kategori->findByID($_id);
?>

<div class="row">
	<div class="col-md-12">
 		<div class="panel panel-default">
 			<div class="panel-heading">
 				<h3 class="panel-title">View Kategori Riset</h3>
 			</div>
 		
 		<div class="panel-body">
 			<table class="table">

 			  <tr>
 				<td class="active">Nama Lengkap</td>
 				<td>:</td>
 				<td><?php echo $data['nama']?></td>
			 </tr>
		
		 	</table>
 		</div>

 
 	<div class="panel-footer">
 		<a class="btn icon-btn btn-success" href="form_kategori.php">
 			<span class="glyphicon btn-glyphicon glyphicon-plus imgcircle text-success"></span>
		 Tambah Kategori
		</a>
 
             	  </div>
 			</div>
	  </div>
</div>


<?php
include_once 'bottom.php';
?>